For writing of program modeling the watch we should describe watch's functionality and interface. The watch is provided to show time which can be changed by user.
The program of the watch should have a display and buttons.

Discribtion of display:
a. There are 5 pages
	1. Main page showing information
	2. Time changing page
	3. Date changing page
	4. Alarm page
 	5. Stopwatch page
b. Display has gray background with black typing.
c. Time format is hh:mm:ss and 24 hours. It should be on the center of display.
d. Date format is dd.mm.yyyy. It is on top-right.
e. Alarm sign is on top-left.

Buttons:
 a. There are Start/Stop, Mode, Down and Up buttons.
 b. "Mode" is used for navigating through pages.
 c. "Start/Stop" is provided for entering and leaving pages.
 d. When "Start/Stop" is pushed for entering page the "Mode" works as jumping button between hours, minutes and seconds (or between dd.mm.yyyy).
 e. "Up" or "Down" is pushed to increase and decrease digits of time and date. Unit of change is 1h for hours, 1m for minutes, 1s for seconds, 1day for days, 1month for months and 1year for years.
 f. "Start/Stop" + "Mode" saves changes.

