#include "MiniComputer.hpp"
#include <string>
#include <iostream>

MiniComputer::MiniComputer(std::string name)
{
    setComputerName(name);
}

std::string
MiniComputer::getComputerName()
{
    return computerName_;
}

void
MiniComputer::setComputerName(std::string name)
{
    if (name.length() > 25) {
        computerName_ = name.substr(0, 25);
        return;
    }

    computerName_ = name;
}

int
MiniComputer::run()
{
    std::cout << getComputerName() << std::endl; /// getter
start:
    showMainMenu();
    int command = getMainCommand();
    if (0 == command) { /// exit command
        return 0;
    }
    int returnValue = executeCommand(command);
    if (0 == returnValue) { /// everything is ok
        goto start;
    }
    return returnValue;
}

void
MiniComputer::showMainMenu()
{
    std::cout << "Command Set\n"
              << "0 – exit\n"
              << "1 – load\n"
              << "2 – store\n"
              << "3 – print\n"
              << "4 – add\n\n"
              << "> Command: ";
}

int
MiniComputer::getMainCommand()
{
    int command;
    std::cin >> command;
    return command;
}

int
MiniComputer::executeCommand(int command)
{
    if (1 == command) { /// load command
        return executeLoadCommand();
    }
    if (2 == command) { /// store command
        return executeStoreCommand();
    }
    if (3 == command) { /// print command
        return executePrintCommand();
    }
    if (4 == command) { /// add command
        return executeAddCommand();
    }
    /// error case
    std::cerr << "Error 1: Command not found!" << std::endl;
    return 1;
}

int
MiniComputer::loadRegister(std::string registerName)
{
    std::cout << "Load into " << registerName << "\n"
              << "Input the value to load into register " << registerName << ".\n"
              << registerName << ": ";
    int number;
    std::cin >> number;
    std::cout << registerName << " = " << number << "\n" << std::endl;
    return number;
}

int
MiniComputer::executeLoadCommand()
{
    std::cout << "Load (into one of the following registers)\n"
              << "0 - a\n"
              << "1 - b\n"
              << "2 - c\n"
              << "3 - d\n\n"
              << "> Register: ";
    int registerNumber;
    std::cin >> registerNumber;
    if (0 == registerNumber) {
        a_ = loadRegister("a");
        return 0;
    }
    if (1 == registerNumber) {
        b_ = loadRegister("b");
        return 0;
    }
    if (2 == registerNumber) {        
        c_ = loadRegister("c");
        return 0;
    }
    if (3 == registerNumber) {
        d_ = loadRegister("d");
        return 0;
    }
    /// error case
    std::cerr << "Error 2: Register not found!" << std::endl;
    return 2;
}

int
MiniComputer::executeStoreCommand()
{
    std::cerr << "Error 127: The store command is not implemented yet" << std::endl;
    return 127;
}

int
MiniComputer::executeAddCommand()
{   
    std::cout << "Add (Register 1 + Register 2 = Register 3)\n"
              << "0 - a\n"
              << "1 - b\n"
              << "2 - c\n"
              << "3 - d\n\n"
              << "> Register1: ";
    
    int registerNumber1;
    int inputRegister1;
    std::string registerLetter1;
    std::cin >> registerNumber1;
    if (registerNumber1 > 3) {
        std::cerr << "Error 2: Register not found" << std::endl;
        return 2;
    }
    if (registerNumber1 < 0) {
        std::cerr << "Error 2: Register not found" << std::endl;
        return 2;
    } 
    if (0 == registerNumber1) {
        inputRegister1 = a_;
        registerLetter1 = "a";
    }
    if (1 == registerNumber1) {
        inputRegister1 = b_;
        registerLetter1 = "b";
    }
    if (2 == registerNumber1) {
        inputRegister1 = c_;
        registerLetter1 = "c";
    }
    if (3 == registerNumber1) {
        inputRegister1 = d_;
        registerLetter1 = "d";
    }
    
    std::string registerLetter2;
    int inputRegister2;
    int registerNumber2;
    std::cout << "> Register2: ";
    std::cin >> registerNumber2;
    if (registerNumber2 > 3) {
        std::cerr << "Error 2: Register not found" << std::endl;
        return 2;
    }
    if (registerNumber2 < 0) {
        std::cerr << "Error 2: Register not found" << std::endl;
        return 2;
    } 
    if (0 == registerNumber2) {
        inputRegister2 = a_;
        registerLetter2 = "a";
    }
    if (1 == registerNumber2) {
        inputRegister2 = b_;
        registerLetter2 = "b";
    }
    if (2 == registerNumber2) {
        inputRegister2 = c_;
        registerLetter2 = "c";
    }
    if (3 == registerNumber2) {
        inputRegister2 = d_;
        registerLetter2 = "d";
    }
   
    int registerNumber3;
    int inputRegister3;
    std::string registerLetter3;
    std::cout << "> Register3: ";
    std::cin >> registerNumber3;
    if (registerNumber3 > 3) {
        std::cerr << "Error 2: Register not found" << std::endl;
        return 2;   
    }
    if (registerNumber3 < 0) {
        std::cerr << "Error 2: Register not found" << std::endl;
        return 2;
    }
    if (0 == registerNumber3) {
        registerLetter3 = "a";
        inputRegister3 = inputRegister1 + inputRegister2;
        a_ = inputRegister3;
    }
    if (1 == registerNumber3) {
        registerLetter3 = "b";
        inputRegister3 = inputRegister1 + inputRegister2;
        b_ = inputRegister3;
    }
    if (2 == registerNumber3) {
        registerLetter3 = "c";
        inputRegister3 = inputRegister1 + inputRegister2;
        c_ = inputRegister3;
    }
    if (3 == registerNumber3) {         
        registerLetter3 = "d";
        inputRegister3 = inputRegister1 + inputRegister2;
        d_ = inputRegister3;
    }
    std::cout << registerLetter3 << " = " << registerLetter1 << " + " << registerLetter2 << " = " << inputRegister1 << " + " << inputRegister2 << " = " << inputRegister3 << "\n" << std::endl;
    return 0;
}

int
MiniComputer::executePrintCommand()
{
    int commandRegister;
    std::cout << "Print (Register or String)\n"
              << "0 – Register\n"
              << "N – String Length\n\n"
              << "> Print: ";
    std::cin >> commandRegister;
    if (0 == commandRegister) {
        std::cout << "Registers\n"
                  << "0 - a\n"
                  << "1 - b\n"
                  << "2 - c\n"
                  << "3 - d\n\n"
                  << "> Register: ";
        int registerNumber;
        std::cin >> registerNumber;
        if (0 == registerNumber) {
            std::cout << a_ << "\n" << std::endl;
            return 0;
        }
        if (1 == registerNumber) {
            std::cout << b_ << "\n" << std::endl;
            return 0;
        }
        if (2 == registerNumber) {
            std::cout << c_ << "\n" << std::endl;
            return 0;
        }
        if (3 == registerNumber) {
            std::cout << d_ << "\n" << std::endl;
            return 0;
        }
        std::cerr << "Error 2: Register not found!" << std::endl;
        return 2;
    }  
    if (commandRegister > 0) {
        std::string line;
		std::cout << "> String: ";
		std::cin.ignore(commandRegister, '\n');
		std::getline(std::cin, line);
		if (line.length() > commandRegister) {
		    line = line.substr(0, commandRegister);
        } 
        start:
            if (line.length() < commandRegister) {
                line += " ";
                goto start;
            }
            std::cout << line << "\n" << std::endl;
            return 0;
    }
    std::cerr << "Error 2: Register not found!" << std::endl;
    return 2;
}
