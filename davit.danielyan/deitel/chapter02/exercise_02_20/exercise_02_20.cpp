#include <iostream>

int
main()
{
    int r;
    std::cout << "Type radius of a circle: ";
    std::cin >> r;  
    if (r <= 0) {
        std::cout << "Error1: The radius of a circle cannot be minus and zero." << std::endl;
        return 1;
    }
    std::cout << "Circle's diameter is " << r * 2 << std::endl;
    std::cout << "Circumference is " << (2 * 3.14159 * r) << std::endl;
    std::cout << "Area is " << (3.14159 * r * r) << std::endl;

    return 0;
}

