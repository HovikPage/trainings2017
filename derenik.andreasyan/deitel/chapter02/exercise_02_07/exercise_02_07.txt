Notice that we placed std:: before cout . This is required when we use nam es 
that we've brought into the program by the preprocessor directive #include
<iostream> .The notation std::cout specifies that we are using a nam e, in this case cout , that belongs to "nam espace" std .
