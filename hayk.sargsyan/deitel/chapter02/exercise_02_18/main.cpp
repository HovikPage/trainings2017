#include <iostream>

int 
main()
{
    int number1, number2;

    std::cout << "Enter the first number: ";
    std::cin >> number1;

    std::cout << "Enter the second number: ";
    std::cin >> number2;

    if(number1 == number2) {
        std::cout << number1 << " is equal to " << number2 << "\n";
    } else if(number1 > number2) {
        std::cout << number1 << " is larger than " << number2 << "\n";
    } else {
        std::cout << number2 << " is larger than " << number1 << "\n";
    }

    return 0;
}
