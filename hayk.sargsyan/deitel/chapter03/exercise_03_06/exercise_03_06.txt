Default constructor has either no parameters, or if it has parameters,
all the parameters have default values. In other words it is a 
constructor which can be called without arguments. With an implicitly 
defined constructor only members of static objects will be reset to 
zero. Also the static properties of classes are reset(only for the 
first created object). Objects defined locally or distributed
dynamically at the start will contain a random set of bits remaining 
in the program stack.
