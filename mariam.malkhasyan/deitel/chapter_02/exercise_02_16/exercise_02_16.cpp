#include <iostream>

int
main()
{
    int number1;
    int number2;

    std::cout << "Enter first integer: ";
    std::cin >> number1;

    std::cout << "Enter second integer: ";
    std::cin >> number2;
	
    if (0 == number2) {
        std::cout << "Error 1: Division by 0, because the denominator cannot be 0. " << std::endl;
        return 1;
    }
	    
    std::cout << number1 << " + " << number2 << " = " << (number1 + number2) << std::endl;
    std::cout << number1 << " - " << number2 << " = " << (number1 - number2) << std::endl;
    std::cout << number1 << " * " << number2 << " = " << (number1 * number2) << std::endl;
    std::cout << number1 << " / " << number2 << " = " << (number1 / number2) << std::endl;
    
    return 0;
}	


