#include "gradeBook.hpp"
#include <iostream>
#include <string>

GradeBook::GradeBook(std::string name, std::string instructorName)
{
    setCourseName(name);
    setInstructorName(instructorName);
}

void 
GradeBook::setCourseName(std::string name)
{
    courseName_ = name;
}

std::string GradeBook::getCourseName()
{
    return courseName_;
}

void
GradeBook::setInstructorName(std::string instructorName)
{
    instructorName_ = instructorName;
}

std::string GradeBook::getInstructorName()
{
    return instructorName_;
}

void
GradeBook::displayMessage()
{
    std::cout << "Welcome to the Gradebook for " << getCourseName()
              << "!" << std::endl;
    std::cout << "This course is presented by: " << getInstructorName() << std::endl;
}
