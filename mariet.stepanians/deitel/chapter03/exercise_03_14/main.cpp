#include "Employee.hpp"
#include <iostream>
#include <string>

int 
main() 
{
    std::string firstName1;
    std::string firstName2;
    std::string lastName1;
    std::string lastName2;
    int salary1 = 0;
    int salary2 = 0;

    Employee employee1(firstName1, lastName1, salary1);
    Employee employee2(firstName2, lastName2, salary2);

    std::cout << "\nYearly Salaries" << std::endl;
    
    salary1 = employee1.getMonthSalary();
    
    std::cout << "\tEnter the first employee: ";
    std::cin  >> firstName1 >> lastName1;
    employee1.setFirstName(firstName1);
    employee1.setLastName(lastName1);
    std::cout << "\tEnter the salary: $";
    std::cin  >> salary1;
    employee1.setMonthSalary(salary1); 

    salary2 = employee2.getMonthSalary(); 
    
    std::cout << "\tEnter the second employee: ";
    std::cin  >> firstName2 >> lastName2;
    employee2.setFirstName(firstName2);
    employee2.setLastName(lastName2);
    std::cout << "\tEnter the salary: $";
    std::cin  >> salary2;
    employee2.setMonthSalary(salary2); 
    
    std::cout << "\nAfter 10% raise" << std::endl;

    employee1.setMonthSalary(salary1 + salary1 / 10);
    employee2.setMonthSalary(salary2 + salary2 / 10);

    salary1 = employee1.getMonthSalary();
    std::cout << "\t" 
              << employee1.getFirstName()
              << " " 
              << employee1.getLastName()
              << ": $"
              << employee1.getMonthSalary() * 12 << std::endl;

    salary2 = employee2.getMonthSalary();
    std::cout << "\t"
              << employee2.getFirstName()
              << " " 
              << employee2.getLastName()
              << ": $"
              << employee2.getMonthSalary() * 12 << std::endl;
    
    return 0;
}

