#include <iostream>

int
main()
{
    int value = 1;

    std::cout << "N\t10*N\t100*N\t1000*N\n\n";
    while (value <= 5) {
        std::cout << value
                  << "\t"
                  << 10 * value
                  << "\t"
                  << 100 * value
                  << "\t"
                  << 1000 * value
                  << "\n";
        ++value;
    }
    return 0;
}
