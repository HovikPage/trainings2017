#include "Gradebook.hpp"
#include <iostream>
#include <string>

int 
main()
{
    std::string courseName;
    std::string teacherName;

    std::cout << "Insert course name: ";
    std::getline(std::cin, courseName);
    std::cout << "Insert teacher name: ";
    std::getline(std::cin, teacherName);

    GradeBook gradeBook1(courseName, teacherName);
    gradeBook1.displayMessage();
   
    std::cout << "Insert course name: ";
    std::getline(std::cin, courseName);
    std::cout << "Insert teacher name: ";
    std::getline(std::cin, teacherName);

    GradeBook gradeBook2(courseName, teacherName);
    gradeBook2.displayMessage();

    return 0; 
}
