#include <iostream>
#include <iomanip>
#include <unistd.h>

int 
main()
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter sales in dollars (-1 to end): ";
        }

        double weekSale;
        std::cin >> weekSale;
        if (-1 == weekSale) {
            return 0;
        }
        if (weekSale < 0) {
            std::cout << "Error 1: wrong week sale." << std::endl;
            return 1;
        }

        std::cout << "Salary is: $" << std::setprecision(2) << std::fixed
                  << (weekSale * 9 / 100 + 200) << "\n" << std::endl;
    }

    return 0;
}
