#include <iostream>

int
main()
{
    int productOfOdd = 1;
    for (int oddNumber = 1; oddNumber <= 15; oddNumber += 2) {
        productOfOdd *= oddNumber;
    }

    std::cout << productOfOdd;
    return 0;    
}
