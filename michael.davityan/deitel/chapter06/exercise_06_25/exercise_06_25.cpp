#include <iostream>
#include <cassert>

int quotient(const int number, const int divisor);
int remainder(const int number, const int divisor);
void separateDigits(const int number);

int
main()
{
    int number;
    std::cin >> number;
    if (number < 1) {
        std::cerr << "Error 1: the entered number is negative or 0.\n";
        return 1;
    }  
    separateDigits(number);

    return 0;
}

int 
quotient(const int number, const int divisor) 
{
    assert(divisor != 0);
    return number / divisor;
}

int 
remainder(const int number, const int divisor) 
{
    assert(divisor != 0);
    return number % divisor;
}

void 
separateDigits(const int number)
{
    assert(number >= 1);
    int numberCopy = number;
    int degree = 1;
    const int RADIX = 10;
    do {
        numberCopy = quotient(numberCopy, RADIX);
        degree *= RADIX;        
    } while (numberCopy != 0); 

    degree = quotient(degree, RADIX);
    numberCopy = number;
    do {
        std::cout << quotient(numberCopy, degree) << "  ";
        numberCopy = remainder(numberCopy, degree);
        degree = quotient(degree, RADIX);
    } while (degree != 0);
    std::cout << std::endl;
}

