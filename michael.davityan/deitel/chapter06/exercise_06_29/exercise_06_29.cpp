#include <iostream>
#include <cmath>
#include <cassert>

bool perfect(const int number);
void printPerfectNumberDivisors (const int number);

int
main()
{
    int numberLimit;
    std::cin >> numberLimit;
    if (numberLimit < 0) {
        std::cout << "Error 1: wrong number limit." << std::endl;
        return 1;
    }
    for (int number = 1; number <= numberLimit; ++number) {
        if (perfect(number)) {
            printPerfectNumberDivisors(number);    
        }
    }
    
    return 0;
}

bool
perfect(const int number)
{
    assert(number > 0);
    int summOfDivisors = 0;
    const int numberLimit = number / 2;
    for (int divisor = 1; divisor <= numberLimit; ++divisor) {
        if (0 == number % divisor) {
            summOfDivisors += divisor;
        }
    } 
    return  number == summOfDivisors;
}

void 
printPerfectNumberDivisors(const int number) 
{
    assert(number > 0);
    std::cout << "Perfect Number: " << number << "\t" << "Divisors:";
    const int numberLimit = number / 2;
    for (int divisor = 1; divisor <= numberLimit; ++divisor) {
        if (0 == number % divisor) {
            std::cout << " " << divisor;
        }
    }
    std::cout << std::endl;
}
