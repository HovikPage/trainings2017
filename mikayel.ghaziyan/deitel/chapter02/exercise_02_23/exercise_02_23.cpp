/// Mikayel Ghaziyan
/// 12/10/2017
/// Exercise 2.22

#include <iostream>

int  /// Beginnign the main function
main()
{
    /// Declaring varioables
    int number1;
    int number2;
    int number3;
    int number4;
    int number5;

    /// Obtaning the values from the user
    std::cout << "Please enter five numbers: \n";
    std::cout << "Numeber 1: ";
    std::cin >> number1;
    std::cout << "Numeber 2: ";
    std::cin >> number2;
    std::cout << "Numeber 3: ";
    std::cin >> number3;
    std::cout << "Numeber 4: ";
    std::cin >> number4;
    std::cout << "Numeber 5: ";
    std::cin >> number5;

    /// Evaluating the minimum and the maximum values among the numbers
    /// MINIMUM
     
    /// Assigning min to the fist number
    int min = number1;

    if (number2 < min) {
        min = number2;
    }
    if (number3 < min) {
        min = number3;
    }
    if (number4 < min) {	
        min = number4;
    } 
    if (number5 < min) {
        min = number5;
    }
    
    /// Displaying the result for the minmum
    std::cout << "The smallest numebr is " << min << ".\n";

    /// MAXIMUM
    
    /// Assigning max to the first number
    int max = number1;

    if (number2 > max) {
        max = number2;
    }
    if (number3 > max) {
        max = number3;
    }
    if (number4 > max) {   
        max = number4;
    }
    if (number5 > max) {
        max = number5;
    }
    
    /// Displaying the result for the maximum
    std::cout << "The largest number is " << max << ".\n";
    
    return 0; /// Successfull complition of the program
}  /// End of the main function


/// End of the .cpp file
 
