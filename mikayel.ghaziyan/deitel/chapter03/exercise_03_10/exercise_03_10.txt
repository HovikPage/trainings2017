Mikayel Ghaziyan
06/11/2017
Exercise 3.10

Classes typically provide set functions and get functions for a data manipulations. The set function is supposed to validate the data that it gets. The get function usually returns the value of a data member. Both functions are capable to keep the privacy from direct external implementation. 

