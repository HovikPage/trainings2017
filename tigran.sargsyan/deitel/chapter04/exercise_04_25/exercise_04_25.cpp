#include <iostream>

int
main()
{
    int length;
    std::cout << "Enter length: ";
    std::cin >> length;

    if (length < 1) {
        std::cout << "Error 1. Entered number is not from 1 to 20." << std::endl;
        return 1;
    }
    if (length > 20) {
        std::cout << "Error 1. Entered number is not from 1 to 20." << std::endl;
        return 1;
    }

    int row = 1;
    while (row <= length) {
        int column = 1;
        while (column <= length) {
            if (1 == row) {
                std::cout << "*";
            } else if (row == length) {
                std::cout << "*";
            } else if (1 == column) {
                std::cout << "*";
            } else if (column == length) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
            ++column;
        }
        std::cout << std::endl;
        ++row;
    }
    return 0;
}

