#include <iostream>

int
main()
{
    int accuracy;
    std::cout << "Enter accuracy: ";
    std::cin >> accuracy;

    if (accuracy < 0) {
        std::cerr << "Error 1. Entered number should be non-negative." << std::endl;
        return 1;
    }

    int current = 0;
    double exponent = 0, factorial = 1;
    while (current <= accuracy) {
        exponent += (1 / factorial);
        ++current;
        factorial *= current;
    }

    std::cout << "Exponent value: " << exponent << std::endl;
    return 0;
}

