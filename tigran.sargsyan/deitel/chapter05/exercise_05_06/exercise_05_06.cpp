#include <iostream>

int
main()
{
    int counter = 0, sum = 0;
    while (true) {
        int number;
        std::cout << "Enter number (9999 to quit): ";
        std::cin >> number;

        if (9999 == number) {
            break;
        }

        sum += number;
        ++counter;
    }

    if (0 == counter) {
        std::cout << "No number entered." << std::endl;
    } else {
        double average = static_cast<double>(sum) / counter;
        std::cout << "Average: " << average << std::endl;
    }

    return 0;
}

