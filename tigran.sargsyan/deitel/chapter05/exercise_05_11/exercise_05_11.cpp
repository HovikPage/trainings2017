#include <iostream>
#include <iomanip>

int
main()
{
    std::cout << "Year" << std::setw(21) << "Amount of deposit" << std::endl;
    std::cout << std::fixed << std::setprecision(2);

    for (int rate = 5; rate <= 10; ++rate) {
        std::cout << "Rate: " << rate << "%" << std::endl;
        double ratePercent = static_cast<double>(rate) / 100;
        double initialAccumulation = 1.0 + ratePercent;
        double principal = 1000.0;
        for (int year = 1; year <= 10; ++year) {
            double amount = principal * initialAccumulation;
            principal *= initialAccumulation;
            std::cout << std::setw(4) << year << std::setw(21) << amount << std::endl;
        }
        std::cout << std::endl;
    }

    return 0;
}

