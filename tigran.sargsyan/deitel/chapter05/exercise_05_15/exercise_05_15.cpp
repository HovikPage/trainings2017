#include "GradeBook.hpp"
#include <iostream>
#include <string>

int
main()
{
    GradeBook myGradeBook("CS101 C++ Programming");
    myGradeBook.displayMessage();
    myGradeBook.inputGrades();
    myGradeBook.displayGradeReport();
    myGradeBook.displayAverageGrade();
    return 0;
}

