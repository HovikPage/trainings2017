#include <iostream>
#include <cmath>

int
main()
{
    int size;
    std::cout << "Enter size: ";
    std::cin >> size;
    if (size < 1 || size > 19 || size % 2 == 0) {
        std::cerr << "Error 1. Wrong size." << std::endl;
        return 1;
    }
    int halfSize = size / 2;
    for (int row = -halfSize; row <= halfSize; ++row) {
        for (int column = -halfSize; column <= halfSize; ++column) {
            std::cout << (std::abs(column) + std::abs(row) <= halfSize ?  "*" : " " );
        }
        std::cout << std::endl;
    }
    return 0;
}

