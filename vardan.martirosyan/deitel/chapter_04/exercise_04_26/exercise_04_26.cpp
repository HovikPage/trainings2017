#include <iostream>

int
main ()
{
    int number;

    std::cout << "Enter five-digit number: ";
    std::cin >> number;

    if (number <= 9999) {
        std::cout << "Error 1: your figure not five-digit." << std::endl;
        return 1;
    }
    if (number >= 100000) {
        std::cout << "Error 1: your figure not five-digit." << std::endl;
        return 1;
    }

    int digit1 = (number / 10000);
    int digit2 = ((number / 1000) % 10);
    int digit3 = ((number / 100) % 10);
    int digit4 = ((number / 10) % 10);
    int digit5 = (number % 10);

    if (digit1 == digit5) {
        if (digit2 == digit4) {
            std::cout << "Number is polindrom." << std::endl;
            return 0;
        }
    }
    std::cout << "Number is not polindrom." << std::endl;
    return 0;
}

