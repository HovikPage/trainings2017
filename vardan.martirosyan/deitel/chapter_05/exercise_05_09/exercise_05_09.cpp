#include <iostream>

int
main()
{
    int total = 1;
    for (int i = 1; i <= 15; i += 2) {
        total *= i;
    }
    std::cout << "Total is " << total << std::endl;
    return 0;
}
